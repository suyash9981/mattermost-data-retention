# Mattermost Data Retention

This Mattermost Data Retention script having following functions :-
1. Mattermost Data Retention - It deletes mattermost after 15 days to preserve mattermost space.
2. Mattermost Channel Data Retention - It preseve data of particular Channels which is preserved permanently.
3. Mattermost Disk Quota - It helps to provide channel user disk usage limitation, default is 20MB.
4. Mattermost Audit Logs - It creates a valuable logs for audit purpose.